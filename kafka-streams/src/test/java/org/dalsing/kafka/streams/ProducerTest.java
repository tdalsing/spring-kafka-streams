package org.dalsing.kafka.streams;

import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;
import java.util.Random;
import java.util.UUID;

@Slf4j
public class ProducerTest {

   Producer<String, GenericRecord> producer;
   Schema schema;
   static Random random = new Random();

   @BeforeEach
   void setUp() throws Exception {
      log.info("setUp");
      schema = new Schema.Parser().parse(getClass().getClassLoader().getResourceAsStream("schema.avsc"));

      Properties config = new Properties();

      config.setProperty("bootstrap.servers", "localhost:9092");
      config.setProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
      config.setProperty("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
      config.setProperty("schema.registry.url", "http://localhost:8082");

      producer = new KafkaProducer<>(config);
   }

   @Test
   public void send() {
      log.info("send");

      try {
         for (int i = 0; i < 100; ++i) {
            String id = UUID.randomUUID().toString();

            GenericRecord record = new GenericData.Record(schema);

            record.put("id", id);
            record.put("name", UUID.randomUUID().toString());
            record.put("count", random.nextInt(100));
            record.put("date", System.currentTimeMillis());
            record.put("amount", random.nextDouble());

            producer.send(new ProducerRecord<>("test", id, record));
         }

         log.info("send: done");
      } catch (Exception e) {
         log.error("send: e={}", e.toString(), e);
         throw e;
      }
   }
}
