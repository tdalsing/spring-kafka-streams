package org.dalsing.kafka.streams.config;

import lombok.extern.slf4j.Slf4j;
import org.dmg.pmml.PMML;
import org.jpmml.model.JAXBUtil;
import org.jpmml.model.filters.ImportFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xml.sax.InputSource;

import javax.xml.transform.Source;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;

@Configuration
@Slf4j
public class PMMLConfig {

   @Bean
   public PMML pmml(@Value("${pmml.file:pmml.xml}") String fileName) throws Exception {
      log.info("pmml: fileName={}", fileName);
      byte[] bytes = Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource(fileName).toURI()));
      String xml = new String(bytes);
      Source source = ImportFilter.apply(new InputSource(new StringReader(xml)));
      return JAXBUtil.unmarshalPMML(source);
   }
}
