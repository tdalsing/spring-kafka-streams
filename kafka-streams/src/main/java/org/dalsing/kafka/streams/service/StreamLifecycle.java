package org.dalsing.kafka.streams.service;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.SmartLifecycle;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

@Component
@FieldDefaults(level = AccessLevel.PRIVATE)
@Slf4j
public class StreamLifecycle implements SmartLifecycle, ApplicationContextAware {

    boolean running = false;
    final List<KafkaStreams> streams = new ArrayList<>();
    final Environment env;
    ApplicationContext applicationContext;

    public StreamLifecycle(Environment env) {
        this.env = env;
    }

    @Override
    public boolean isAutoStartup() {
        return true;
    }

    @Override
    public void stop(Runnable runnable) {
        stop();
    }

    @Override
    public void start() {
        log.info("start");

        String[] definitions = applicationContext.getBeanNamesForType(StreamDef.class);
        log.info("start: definitions={}", Arrays.toString(definitions));

        for (String definition : definitions) {
            log.info("start: definition={}", definition);

            StreamDef streamDef = applicationContext.getBean(definition, StreamDef.class);
            Topology topology = streamDef.getTopology();

            Properties config = new Properties();
            copy(env, config);
            config.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, streamDef.getApplicationId());
            config.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, streamDef.getKeySerdeClass());
            config.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, streamDef.getValueSerdeClass());

            log.info("start: config={}", config);

            KafkaStreams ks = new KafkaStreams(topology, config);
            ks.start();

            streams.add(ks);
        }

        running = true;
    }

    @Override
    public void stop() {
        log.info("stop");
        running = false;

        for (KafkaStreams stream : streams) {
            stream.close();
        }

        streams.clear();
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    @Override
    public int getPhase() {
        return 0;
    }

    private void copy(Environment env, Properties config) {
        copy(env, config, StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        copy(env, config, AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8082");
    }

    private void copy(Environment env, Properties config, String name, String def) {
        config.setProperty(name, env.getProperty(name, def));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
