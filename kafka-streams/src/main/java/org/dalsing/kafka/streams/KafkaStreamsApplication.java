package org.dalsing.kafka.streams;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaStreamsApplication {

    public static void main(String[] args) {
        new SpringApplication(KafkaStreamsApplication.class).run(args);
    }
}
