package org.dalsing.kafka.streams.service;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.apache.kafka.streams.Topology;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class StreamDef {

    Topology topology;
    String applicationId;
    String keySerdeClass;
    String valueSerdeClass;
 }
