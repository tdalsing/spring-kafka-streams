package org.dalsing.kafka.streams.config;

import com.datastax.driver.mapping.Mapper;
import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.generic.GenericRecord;
import org.apache.ignite.IgniteCache;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.dalsing.kafka.streams.service.KStreamFluxHelper;
import org.dalsing.kafka.streams.service.StreamDef;
import org.dalsing.kafka.streams.data.TestValue;
import org.dmg.pmml.FieldName;
import org.dmg.pmml.OpType;
import org.dmg.pmml.PMML;
import org.jpmml.evaluator.FieldValue;
import org.jpmml.evaluator.InputField;
import org.jpmml.evaluator.ModelEvaluator;
import org.jpmml.evaluator.ModelEvaluatorFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.*;

@Configuration
@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class StreamConfig {

   Mapper<TestValue> testValueMapper;
   IgniteCache<String, TestValue> cache;
   PMML pmml;
   static ModelEvaluatorFactory modelEvaluatorFactory = ModelEvaluatorFactory.newInstance();


   public StreamConfig(Mapper<TestValue> testValueMapper, IgniteCache<String, TestValue> cache, PMML pmml) {
      this.testValueMapper = testValueMapper;
      this.cache = cache;
      this.pmml = pmml;
   }

   @Bean
   public StreamDef topology1() {
      log.info("topology1");

      StreamsBuilder builder = new StreamsBuilder();
      KStream<String, GenericRecord> stream = builder.stream("test");

      Flux<GenericRecord> flux = KStreamFluxHelper.createFlux(stream);

      flux.map(value -> convert(value))
            .map(testValueMapper::saveAsync)
            .bufferTimeout(100, Duration.ofSeconds(10))
            .subscribe(list -> list.forEach(value -> {
                     try {
                        value.get();
                     } catch (Exception e) {
                        log.error("topology1:forEach: e={}", e.toString(), e);
                     }
                  })
            );

      Topology topology = builder.build();

      return StreamDef.builder()
            .topology(topology)
            .applicationId("topology1")
            .keySerdeClass(Serdes.String().getClass().getName())
            .valueSerdeClass(GenericAvroSerde.class.getName())
            .build();
   }

   private TestValue convert(GenericRecord record) {
      return TestValue.builder()
            .id(record.get("id").toString())
            .name(record.get("name").toString())
            .amount((Double) record.get("amount"))
            .count((Integer) record.get("count"))
            .date(new Date((Long) record.get("date")))
            .build();
   }

   private Map<String, TestValue> toMap(List<TestValue> list) {
      Map<String, TestValue> map = new HashMap<>();
      list.forEach(value -> map.put(value.getId(), value));
      return map;
   }

   @Bean
   public StreamDef topology2() {
      log.info("topology2");

      StreamsBuilder builder = new StreamsBuilder();
      KStream<String, GenericRecord> stream = builder.stream("test");

      Flux<GenericRecord> flux = KStreamFluxHelper.createFlux(stream);

      flux.map(value -> convert(value))
            .bufferTimeout(100, Duration.ofSeconds(10))
            .map(list -> toMap(list))
            .subscribe(map -> cache.putAll(map));

      Topology topology = builder.build();

      return StreamDef.builder()
            .topology(topology)
            .applicationId("topology2")
            .keySerdeClass(Serdes.String().getClass().getName())
            .valueSerdeClass(GenericAvroSerde.class.getName())
            .build();
   }

   private void predict(TestValue value) {
      double amount = value.getAmount();
      ModelEvaluator<?> evaluator = modelEvaluatorFactory.newModelEvaluator(pmml);
      Map<FieldName, FieldValue> arguments = new LinkedHashMap<>();
      InputField inputField = evaluator.getInputFields().get(0);
      FieldName fn = inputField.getName();
      FieldValue fv = inputField.prepare(amount);
      arguments.put(fn, fv);
      Map<FieldName, ?> results = evaluator.evaluate(arguments);
      Object result = results.values().iterator().next();
      log.info("predict: amount={}, result={}, value={}",amount, result, value);
   }

   @Bean
   public StreamDef topology3() {
      log.info("topology3");

      StreamsBuilder builder = new StreamsBuilder();
      KStream<String, GenericRecord> stream = builder.stream("test");
      stream
            .mapValues(this::convert)
            .foreach((key, value) -> predict(value));

      Topology topology = builder.build();

      return StreamDef.builder()
            .topology(topology)
            .applicationId("topology3")
            .keySerdeClass(Serdes.String().getClass().getName())
            .valueSerdeClass(GenericAvroSerde.class.getName())
            .build();
   }

   @Bean
   public StreamDef topology4() {
      log.info("topology4");

      StreamsBuilder builder = new StreamsBuilder();
      KStream<String, GenericRecord> stream = builder.stream("test");
      stream.foreach((key, value) -> log.info("topology4: key={}, value={}", key, value));

      Topology topology = builder.build();

      return StreamDef.builder()
            .topology(topology)
            .applicationId("topology4")
            .keySerdeClass(Serdes.String().getClass().getName())
            .valueSerdeClass(GenericAvroSerde.class.getName())
            .build();
   }
}
