package org.dalsing.kafka.streams.service;

import org.apache.kafka.streams.kstream.KStream;
import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Flux;

public class KStreamFluxHelper {

   public static <K,V> Flux<V> createFlux(KStream<K,V> stream) {
      EmitterProcessor<V> emitter = EmitterProcessor.create();
      Flux<V> flux = Flux.from(emitter);

      stream.foreach((key, value) -> emitter.onNext(value));
      return flux;
   }

   private KStreamFluxHelper() {
   }
}
