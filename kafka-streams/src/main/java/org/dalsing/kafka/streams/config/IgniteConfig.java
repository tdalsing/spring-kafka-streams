package org.dalsing.kafka.streams.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;
import org.dalsing.kafka.streams.data.TestValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;

@Configuration
@Slf4j
public class IgniteConfig {

   @Bean
   public Ignite ignite(@Value("${ignite.host:localhost}") String host) {
      log.info("ignite: host={}", host);
      Ignition.setClientMode(true);

      TcpDiscoveryVmIpFinder finder = new TcpDiscoveryVmIpFinder();
      finder.setAddresses(Collections.singleton(host));

      TcpDiscoverySpi tcp = new TcpDiscoverySpi();
      tcp.setIpFinder(finder);

      IgniteConfiguration configuration = new IgniteConfiguration();
      configuration.setDiscoverySpi(tcp);

      return Ignition.start(configuration);
   }

   @Bean
   public IgniteCache<String, TestValue> cache(Ignite ignite) {
      log.info("cache");
      CacheConfiguration<String, TestValue> cfg = new CacheConfiguration<>();
      cfg.setName("test");
      cfg.setCacheMode(CacheMode.REPLICATED);
      return ignite.getOrCreateCache(cfg);
   }
}
