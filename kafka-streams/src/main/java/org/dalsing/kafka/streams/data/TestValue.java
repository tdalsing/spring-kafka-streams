package org.dalsing.kafka.streams.data;

import com.datastax.driver.mapping.annotations.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Table(name = "test")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TestValue {

    String id;
    String name;
    int count;
    Date date;
    double amount;
}
