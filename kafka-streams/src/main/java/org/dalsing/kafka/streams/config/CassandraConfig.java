package org.dalsing.kafka.streams.config;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import lombok.extern.slf4j.Slf4j;
import org.dalsing.kafka.streams.data.TestValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class CassandraConfig {

    @Bean
    public Cluster cluster(@Value("${cassandra.host:localhost}") String host) {
        log.info("cluster: host={}", host);
        return Cluster.builder().addContactPoint(host).build();
    }

    @Bean
    public Session session(Cluster cluster, @Value("${cassandra.keyspace:test}") String keyspace) {
        log.info("session: keyspace={}", keyspace);
        return cluster.connect(keyspace);
    }

    @Bean
    public MappingManager mappingManager(Session session) {
        log.info("mappingManager");
        return new MappingManager(session);
    }

    @Bean
    public Mapper<TestValue> testValueMapper(MappingManager mappingManager) {
        log.info("testValueMapper");
        return mappingManager.mapper(TestValue.class);
    }
}
